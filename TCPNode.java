/*
 * @(#)TCPNode.java
 * Time-stamp: "2007-11-06 18:18:06 anton"
 */

// import necessary input/output classes
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;

// import necessary network classes
import java.net.ConnectException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * TCPNode &ndash; a node in a TCP packet ring.
 *
 * @author "Anton Johansson" <anton.johansson@gmail.com>
 * @author "Victor Zamanian" <victor.zamanian@gmail.com>
 */
public class TCPNode extends Node {

    private ServerSocket welcomeSocket;

    private Socket nextNode;
    private Socket prevNode;

    private DataOutputStream nextNodeOutput;
    private InputStream prevNodeInput;

    /**
     * Creates a new TCPNode instance with all necessary information.
     *
     * @param localPort the local port to listen to
     * @param nextHost the host to connect to
     * @param nextPort the port that <code>nextHost</code> listens to
     * @param logger determines if this node should log lap-times
     */
    public TCPNode(int localPort,
                   InetAddress nextHost,
                   int nextPort,
                   boolean logger) {

        super(localPort, nextHost, nextPort, logger);

        int tries = 0;
        final int MAXTRIES = 120;

        // create a server socket so a node can connect to this one
        try {
            welcomeSocket = new ServerSocket(localPort);
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        // first try to make contact with
        // the next and previous node in the ring
        try {
            nextNode = new Socket(nextHost, nextPort);
            prevNode = welcomeSocket.accept();
        }
        catch (IOException e) {
            System.out.println(e);
        }

        // if connecting to the next node failed
        if (nextNode == null) {
            // try to let the previous node connect to this node
            try {
                prevNode = welcomeSocket.accept();
            }
            catch (IOException e) {
                e.printStackTrace();
            }

            // then try to connect to the next node a number of times
            while (nextNode == null && tries < MAXTRIES) {
                try {
                    nextNode = new Socket(nextHost, nextPort);
                }
                catch (IOException e) {
                    System.out.println(e);
                }
                // increase the number of tries made by 1
                tries++;
                try {
                    Thread.sleep(250);
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        // if we after all of these efforts have been unsuccessful
        // in connecting to the next and previous node, exit
        // with a status code of 1, indicating failure.
        if (nextNode == null || prevNode == null) {
            System.out.println("Unable to connect; quitting...");
            System.exit(1);
        }
        else {
            try {
              nextNodeOutput = new DataOutputStream(
                                                   nextNode.getOutputStream());
              prevNodeInput  = prevNode.getInputStream();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }

        // if this node is a logger, set the file name
        // and open a file with that name for writing.
        if (isLogger) {
            logFileName = "lap-times-tcp.log";
            try {
                file = new FileWriter(logFileName);
                out = new BufferedWriter(file);
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Runs the main logic of the TCPNode
     */
    public void runTCPNode() {
        // the byte-array to store received data in
        byte[] receivedData = new byte[DATAGRAMSIZE];
        // the current phase the ring is in
        int phase;

        //send the first packet
        try {
            nextNodeOutput.write(constructByteArray(
                                      FINDMASTER, new Integer(ID).toString()));
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        // PHASE ONE
        phase = FINDMASTER;
        while (phase == FINDMASTER) {
            // receive packet
            try {
                prevNodeInput.read(receivedData);
            }
            catch (IOException e) {
                e.printStackTrace();
            }

            // acquire the message's message type
            // also set the phase to the message type, because
            // if it changes (to RUNMODE), that means another
            // node was chosen as the master node.
            int messageType = phase = receivedData[0];

            // if the message received was a message of type RUNMODE
            if (phase == RUNMODE) {
                // send a message to the following node(s),
                // telling it to break out of phase one.
                try {
                    nextNodeOutput.write(receivedData);
                    sentPackets++;
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
                // if the start time hasn't been set
                if (isLogger && startTime == 0) {
                    // set the starting time -- the time
                    // when phase two was initiated
                    startTime = System.nanoTime();
                }
                // break out of phase one
                break;
            }

            // acquire the message's length
            int messageLength = receivedData[1];

            // acquire the message...
            String message =
                new String(receivedData).substring(2, messageLength + 2);

            // the largest ID that this packet has seen so far, i.e. the
            // ID of the node with the largest ID so far in the ring.
            int receivedID = Integer.parseInt(message);

            // if this node will surely not be the master node because
            // there exists a node with an ID larger than this one's 
            if (receivedID > ID) {
                // just forward the data to the next node
                try {
                    nextNodeOutput.write(receivedData);
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }

            // if this node should be the master node
            else if (receivedID == ID) {
                // set this node to be the master node
                isMasterNode = true;
                // set the phase to RUNMODE
                phase = RUNMODE;
            }
        } // end while

        // if this node turned out to be the master node,
        // send the first message of phase two.
        if (isMasterNode) {
            // send first packet in phase two
            try {
                nextNodeOutput.write(constructByteArray(
                                        RUNMODE, new Integer(ID).toString()));
                // set the start time if it hasn't been set
                if (isLogger && startTime == 0) {
                    startTime = System.nanoTime();
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }

            // increase the amount of sent packets by 1
            sentPackets++;
        }

        // PHASE TWO
        while (phase == RUNMODE) {
            // receive data from previous node
            try {
                prevNodeInput.read(receivedData);
            }
            catch (IOException e) {
                e.printStackTrace();
            }

            // if this node is a logging node,
            if (isLogger) {
                // write the current average lap time of the data
                writeToLog(new String(receivedData).substring(2, receivedData[1] + 2));
            }

            // send data to the next node
            try {
                nextNodeOutput.write(receivedData);
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            sentPackets++;

            // if it is time to end phase two
            if (sentPackets == PACKETSTOSEND) {
                // set the phase to phase three
                // and send a terminating packet
                phase = TERMINATE;
                if (isMasterNode) {
                    try {
                        // send terminate packet
                        nextNodeOutput.write(
                                        constructByteArray(TERMINATE, null));
                        // receive terminate packet before
                        // exiting the ring by closing socket
                        prevNodeInput.read(receivedData);
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        } // end while

        // close sockets (and log file if this node is a logger)
        try {
            nextNode.close();
            prevNode.close();
            if (isLogger) {
                System.out.println("Closing log file...");
                closeLogFile();
                System.out.println("Finished.");
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method is not used...
     *
     * @param receivePacket
     * @return
     */
    public boolean receivePacket(DatagramPacket receivePacket) {
        return false; // not used, but interfaces can be demanding...
    }

    /**
     * This method is not used...
     *
     * @param receivePacket
     * @return
     */
    public boolean sendPacket(DatagramPacket receivePacket) {
        return false; // not used, but interfaces can be demanding...
    }

    /**
     * For starting the TCPNode from a commandline
     *
     * @param args should contain local-port next-host next-port [L]
     *             where the L is optional and specifies if the node
     *             should log lap-times
     * @exception IOException if an error occurs
     */
    public static void main(String[] args) throws IOException,
                                                  InterruptedException {
        TCPNode node = null;
        // determine if this node should be a logger
        boolean logger = false;
        if (args.length == 4 && args[3].charAt(0) == 'L') {
            logger = true;
        }

        // create a new node
        node = new TCPNode(Integer.parseInt(args[0]),
                           InetAddress.getByName(args[1]),
                           Integer.parseInt(args[2]),
                           logger);
        node.runTCPNode();
    }
}