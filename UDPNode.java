/*
 * @(#)UDPNode.java
 * Time-stamp: "2007-11-06 18:12:55 anton"
 */

import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.IOException;

import java.net.InetAddress;
import java.net.DatagramSocket;
import java.net.DatagramPacket;
import java.net.SocketException;

/**
 * UDPNode &ndash; a node in a UDP packet ring.
 *
 * @author "Anton Johansson" <anton.johansson@gmail.com>
 * @author "Victor Zamanian" <victor.zamanian@gmail.com>
 */
public class UDPNode extends Node {

    // This node's socket
    private DatagramSocket mySocket = null;

    /**
     * Creates a new UDPNode instance with all necessary information.
     *
     * @param localPort the local port to listen to
     * @param nextHost the host to connect to
     * @param nextPort the port that <code>nextHost</code> listens to
     * @param logger determines if this node should log lap-times
     */
    public UDPNode(int localPort,
                   InetAddress nextHost,
                   int nextPort,
                   boolean logger) {

        // initiate fields
        super(localPort, nextHost, nextPort, logger);
        // open a DatagramSocket
        try {
            this.mySocket = new DatagramSocket(localPort);
        }
        catch (SocketException e) {
            System.out.println(e);
            System.exit(1);
        }

        // if this node is a logging node
        if (isLogger) {
            logFileName = "lap-times-udp.log";
            try {
                file = new FileWriter(logFileName);
                out = new BufferedWriter(file);
            }
            catch (IOException e) {
                System.out.println(e);
            }
        }
    } // end constructor

    /**
     * Runs the main logic of the UDPNode
     */
    public void runUDPNode() {

        // keeps track of whether or not the
        // main while-loop should be running
        boolean runMainWhileLoop = true;

        // start phase 1 by sending out a find-master query
        sendPacket(constructPacket(FINDMASTER, new Integer(ID).toString()));

        // initate fields used to store information from arrived packets
        byte[] receivedData = new byte[DATAGRAMSIZE];
        DatagramPacket receivePacket =
            new DatagramPacket(receivedData, DATAGRAMSIZE);

        // start receiving packets ('main while-loop')
        while (runMainWhileLoop) {
            // Waiting for data to arrive
            receivePacket(receivePacket);

            // acquire the message's message type
            int messageType = receivedData[0];

            // keeping track of which phase we're in
            int phase = FINDMASTER;
            switch (messageType) {
                // if messageType == 1
            case FINDMASTER:

                // acquire the message's length
                int messageLength = receivedData[1];

                // acquire the message...
                String message =
                    new String(receivedData).substring(2, messageLength + 2);

                // the largest ID that this packet has seen so far, i.e. the
                // ID of the node with the largest ID so far in the ring.
                int receivedID = Integer.parseInt(message);

                // if we left phase 1 but messages
                // are still circling the ring:
                if (phase != FINDMASTER) {
                    // don't do anything.
                    break;
                }
                // if this node will surely not be the master node
                if (receivedID > ID) {
                    // set a new receiver for the received packet
                    receivePacket.setPort(nextPort);
                    receivePacket.setAddress(nextHost);
                    sendPacket(receivePacket);
                }
                // if this node has a larger ID than the one of the sending node
                else if (receivedID < ID) {
                    // Send own ID again since it could have been lost
                    sendPacket(constructPacket(FINDMASTER, new Integer(ID).toString()));
                }
                // if this node should be the master node
                else if (receivedID == ID) {
                    phase = RUNMODE;
                    isMasterNode = true;

                    // if this node is a logging node,
                    if (isLogger) {
                        // set the starting time -- the time
                        // when phase two was initiated
                        startTime = System.nanoTime();

                        // increase the number of sent packets.
                        sentPackets++;
                    }
                    // send first packet in phase two
                    sendPacket(constructPacket(RUNMODE, new Integer(ID).toString()));
                }
                break;
            // if messageType == 2
            case RUNMODE:
                // make sure everybody knows we're in the second phase
                phase = RUNMODE;

                // if this node is a logger and the start time hasn't been set
                if (isLogger && startTime == 0) {
                    // set the start time
                    startTime = System.nanoTime();
                }
                // if this node is a logger:
                if (isLogger) {
                    // write to file
                    writeToLog(new String(receivedData).substring(2, receivedData[1] + 2));
                }
                // if this is the master node and
                // we've sent as many packets as we were to (and the
                // number of packets to send is not zero (infinitely many)):
                if (isMasterNode && sentPackets == PACKETSTOSEND) {
                    // send the terminating packet to the next node
                    // (and, indirectly so, to all other nodes)
                    sendPacket(constructPacket(TERMINATE, "3"));
                    break;
                }

                // if we've not send as many packets as we are to:
                // re-route packet (set its port and address) and send it
                receivePacket.setPort(nextPort);
                receivePacket.setAddress(nextHost);
                sendPacket(receivePacket);
                sentPackets++;
                break;
            // if messageType == 3 (stop sending packets and ultimately quit
            case TERMINATE:
                runMainWhileLoop = false;
                // if this node is a logger, log one final time before
                // stopping the whole shebang and close the file
                if (isLogger) {
                    writeToLog(new String(receivedData).substring(2, receivedData[1] + 2));
                    System.out.println("Closing file...");
                    closeLogFile();
                    System.out.println("Finished.");
                }
                // send terminating packet (end the whole shebang)
                sendPacket(constructPacket(TERMINATE, "3"));
                mySocket.close();
                break;
            } // end switch
        } // end main while-loop
    } // end runUDPNode

    /**
     * Sends a DatagramPacket through this UDPNode's DatagramSocket.
     *
     * @return boolean <code>true</code> if sending was successful,
     * else <code>false</code>.
     */
    public boolean sendPacket(DatagramPacket sendPacket) {
        try {
            mySocket.send(sendPacket);
        }
        catch (IOException e) {
            System.out.println(e);
            return false;
        }
        return true;
    }

    /**
     * Receives a DatagramPacket from this UDPNode's DatagramSocket.
     *
     * @return boolean <code>true</code> if receiving was successful,
     * else <code>false</code>.
     */
    public boolean receivePacket(DatagramPacket receivePacket) {
        try {
            mySocket.receive(receivePacket);
        }
        catch (IOException e) {
            System.out.println(e);
            return false;
        }
        return true;
    }
           
    /**
     * For starting the UDPNode from a commandline
     *
     * @param args should contain local-port next-host next-port [L]
     *             where the L is optional and specifies if the node
     *             should log lap-times
     * @exception IOException if an error occurs
     */
    public static void main(String[] args) throws IOException {
        // args should contain local-port next-host next-port
        UDPNode node = null;
        boolean logger = false;
        if (args.length == 4 && args[3].charAt(0) == 'L') {
            logger = true;
        }
        try {
            node = new UDPNode(Integer.parseInt(args[0]),
                               InetAddress.getByName(args[1]),
                               Integer.parseInt(args[2]),
                               logger);
        }
        catch (Exception e) {
            System.out.println("You most likely provided parameters " +
                               "incorrectly to the program." + '\n' + 
                               "Usage: java UDPNode local-port " +
                               "next-host next-port [L]");
            System.exit(1);
        }
        try {
            node.runUDPNode();
        }
        catch (Exception e) {
            System.out.println("Something went wrong... Try making sure " +
                               "that the ports you are trying to use " +
                               "are not occupied by any other program.");
        }
    }
}
