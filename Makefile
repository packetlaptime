.SUFFIXES : .java .class

CLASSES = NodeInterface.class Node.class TCPNode.class UDPNode.class
CFLAGS = -classpath $(CURDIR)

all : $(CLASSES)	

.java.class :
	javac $(CFLAGS) $<

runUDP :
	java $(CFLAGS) UDPNode 9800 localhost 9801 L &
	java $(CFLAGS) UDPNode 9801 localhost 9802 &
	java $(CFLAGS) UDPNode 9802 localhost 9803 &
	java $(CFLAGS) UDPNode 9803 localhost 9804 &
	java $(CFLAGS) UDPNode 9804 localhost 9805 &
	java $(CFLAGS) UDPNode 9805 localhost 9806 &
	java $(CFLAGS) UDPNode 9806 localhost 9807 &
	java $(CFLAGS) UDPNode 9807 localhost 9808 &
	java $(CFLAGS) UDPNode 9808 localhost 9809 &
	java $(CFLAGS) UDPNode 9809 localhost 9800 &

runTCP :
	java $(CFLAGS) TCPNode 9800 localhost 9801 L &
	java $(CFLAGS) TCPNode 9801 localhost 9802 &
	java $(CFLAGS) TCPNode 9802 localhost 9803 &
	java $(CFLAGS) TCPNode 9803 localhost 9804 &
	java $(CFLAGS) TCPNode 9804 localhost 9805 &
	java $(CFLAGS) TCPNode 9805 localhost 9806 &
	java $(CFLAGS) TCPNode 9806 localhost 9807 &
	java $(CFLAGS) TCPNode 9807 localhost 9808 &
	java $(CFLAGS) TCPNode 9808 localhost 9809 &
	java $(CFLAGS) TCPNode 9809 localhost 9800 &

runTCPtry :
	java $(CFLAGS) TCPNode 9800 fyrkant 9801 L &
	ssh fyrkant "java $(CFLAGS) TCPNode 9801 $(HOST) 9802" &
	java $(CFLAGS) TCPNode 9802 kub 9803 &
	ssh kub "java $(CFLAGS) TCPNode 9803 $(HOST) 9804" &
	java $(CFLAGS) TCPNode 9804 localhost 9800 &

runUDPtry :
	java $(CFLAGS) UDPNode 9800 fyrkant 9801 L &
	ssh fyrkant "java $(CFLAGS) UDPNode 9801 $(HOST) 9802" &
	java $(CFLAGS) UDPNode 9802 kub 9803 &
	ssh kub "java $(CFLAGS) UDPNode 9803 $(HOST) 9804" &
	java $(CFLAGS) UDPNode 9804 localhost 9800 &

api :
	javadoc *.java -d doc -charset UTF-8

clean :
	rm -rf $(CLASSES) *.log *~
