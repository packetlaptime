#! /bin/bash
javacflags="-classpath ~/edu/doi/lab2"
numberofnodes=
nodtyp=
firsthost=
secondhost=

function usage {
    echo "Usage: $0 <number of nodes> <node type> <first host> <second host>"
}

if [ "$1" = "" ]
then
    echo No parameters given.
    usage
    exit
elif [ "$2" = "" ]
then
    echo No node type given.
    usage
    exit
elif [ "$3" = "" ]
then
    echo No first host name given.
    usage
    exit
elif [ "$4" = "" ]
then
    echo No second host name given.
    usage
    exit
else
    numberofnodes=$1
    nodtyp=$2
    nodtypupcase=`echo $2 | tr [:lower:] [:upper:]`
    firsthost="$3"
    secondhost="$4"
fi

for ((i=0;i<$numberofnodes-2;i+=2))
do
    echo "java $javacflags "$nodtypupcase"Node `expr 9800 + $i` $secondhost `expr 9800 + $i + 1` &" >> $nodtyp.$numberofnodes.$firsthost.sh
    echo "java $javacflags "$nodtypupcase"Node `expr 9800 + $i + 1` $firsthost `expr 9800 + $i + 2` &" >> $nodtyp.$numberofnodes.$secondhost.sh
done

echo "java $javacflags "$nodtypupcase"Node `expr 9800 + $i` $secondhost `expr 9800 + $i + 1` &" >> $nodtyp.$numberofnodes.$firsthost.sh
echo "java $javacflags "$nodtypupcase"Node `expr 9800 + $i + 1` $firsthost `expr 9800` L &" >> $nodtyp.$numberofnodes.$secondhost.sh
