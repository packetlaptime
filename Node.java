/*
 * @(#)Node.java
 * Time-stamp: "2007-10-30 20:36:14 anton"
 */

import java.util.Random;

import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.IOException;

import java.net.InetAddress;
import java.net.DatagramPacket;
import java.net.SocketException;

/**
 * Node &ndash; a node in a packet ring.
 *
 * @author "Anton Johansson" <anton.johansson@gmail.com>
 * @author "Victor Zamanian" <victor.zamanian@gmail.com>
 */
public abstract class Node implements NodeInterface {

    // general Node information
    protected final int localPort;
    protected final InetAddress nextHost;
    protected final int nextPort;
    protected final int ID;
    // the time when the logging starts
    protected long startTime = 0;

    // keeps track of whether this Node
    // is a master node (sends the first message)
    // and/or a logger (logs lap times to a file)
    protected boolean isMasterNode = false;
    protected boolean isLogger = false;

    // log file fields
    protected String logFileName = null;
    protected FileWriter file = null;
    protected BufferedWriter out = null;

    // different message types
    protected final int FINDMASTER = 1;
    protected final int RUNMODE   = 2;
    protected final int TERMINATE  = 3;
    // message datagram size
    protected final int DATAGRAMSIZE = 100;
    // All IDs have 10 figures and range from
    // 10^9 to the maximum value of an int.
    private final int IDRANGE = Integer.MAX_VALUE - (int) Math.pow(10,9);
    
    // number of packets to send in the ring
    // If PACKETSTOSEND is set to 0, the rings will
    // go on until the program is interrupted.
    protected final int PACKETSTOSEND = 10000;
    // needed to calculate lap times
    protected int sentPackets = 0;

    /**
     * Constructs a new object instance of the Node class.
     */
    public Node(int localPort,
                InetAddress nextHost,
                int nextPort,
                boolean logger) {

        // set some fields
        this.ID        = (int) Math.pow(10,9) + (new Random().nextInt(IDRANGE));
        this.localPort = localPort;
        this.nextHost  = nextHost;
        this.nextPort  = nextPort;
        this.isLogger  = logger;
        // print some information about this node
        System.out.println("\n----------(ID = " + ID + ")----------\n" +
                           "Node listens on port no.: " + localPort + 
                           "\nNext host in ring is: " + nextHost +
                           "\nNext host listens on port no.: " + nextPort);
    }

    /**
     * Sends a DatagramPacket through this Node's socket.
     * NOTE: Since the Node class is really just a container
     * for methods and fields for the subclasses of the Node class,
     * this methods does nothing but return 'false' because the
     * method of sending packets differ between UDPNode and TCPNode.
     *
     * @return boolean Whether or not the sending was successful.
     */
    public abstract boolean sendPacket(DatagramPacket sendPacket);

    /**
     * Receives a DatagramPacket from this Node's socket.
     * NOTE: This method does nothing but return 'false',
     * because: see note at sendPacket(DatagramPacket sendPacket).
     *
     * @return boolean Whether or not the receiving was successful.
     */
    public abstract boolean receivePacket(DatagramPacket receivePacket);


    /**
     * Constructs a byte-array with first the messageType in the first
     * byte and the message in following
     *
     * @param messageType the type of the message
     * @param message the message
     * @return a byte-array first byte is messageType rest is message
     */
    public byte[] constructByteArray(int messageType, String message) {
        if (message == null) {
            message = "";
        }

        byte[] sendBytes = new byte[DATAGRAMSIZE];
        sendBytes[0] = (byte) messageType;
        sendBytes[1] = message.length() < (DATAGRAMSIZE-2) ? (byte) message.length() : (byte) (DATAGRAMSIZE-2);

        for (int i=0; i<sendBytes[1]; i++) {
            sendBytes[2 + i] = (byte) message.charAt(i);
        }
        return sendBytes;
    }
    
    /**
     * Constructs a DatagramPacket with an InetAddress, a port,
     * a message type and a message. The method asserts that the
     * message is not of greater length than the DatagramPacket's
     * maximum length.
     *
     * @return DatagramPacket The packet constructed by this method.
     */
    public DatagramPacket constructPacket(int messageType, String message) {
        byte[] sendBytes = constructByteArray(messageType, message);
        return new DatagramPacket(sendBytes,
                                  sendBytes.length,
                                  nextHost,
                                  nextPort);
    }

    
    /**
     * Writes the lap times to a log file.
     *
     * @return boolean <code>true</code> if writing to the log file
     * was successful, <code>false</code> if not.
     */
    public boolean writeToLog(String id) {
        try {
            out.write("Average time so far: " +
                      (System.nanoTime() -
                       startTime)/(double)sentPackets +
                      " ns, ID = " + id + "\n");
        }
        catch (IOException e) {
            System.out.println(e);
            return false;
        }
        return true;
    }

    /**
     * Closes the log file to which the lap times are written.
     *
     * @return boolean <code>true</code> if closing the file was
     * successful, <code>false</code> if not.
     */
    public boolean closeLogFile() {
        try {
            out.close();
        }
        catch (IOException e) {
            System.out.println(e);
            return false;
        }
        return true;
    }

}
