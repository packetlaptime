set terminal png
set output "graph.png"
set xlabel "Antal noder"
set ylabel "Tid i nanosekunder"

plot 'udp.plot.dat' ti "UDP-varvtider", \
        'tcp.plot.dat' ti "TCP-varvtider"
