/*
 * @(#)NodeInterface.java
 * Time-stamp: "2007-10-30 20:39:29 anton"
 */

import java.net.DatagramPacket;

/**
 * NodeInterface &ndash; an interface for a node in a packet ring.
 *
 * @author "Anton Johansson" <anton.johansson@gmail.com>
 * @author "Victor Zamanian" <victor.zamanian@gmail.com>
 */
public interface NodeInterface { 
    public abstract boolean sendPacket(DatagramPacket sendPacket);
    public abstract boolean receivePacket(DatagramPacket receivePacket);
    public abstract DatagramPacket constructPacket(int messageType, String message);
    public abstract boolean writeToLog(String id);
    public abstract boolean closeLogFile();
}
